﻿using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;
using ToBeNorME.Library.Models;
using ToBeNorME.Library.Repositories;
using ToBeNorME.Library.Services;

namespace ToBeNorMe.Tests.Services
{
    [TestClass]
    public class CategoryServicesTest
    {
        [TestMethod]
        public async Task create_category_async_should_should_invoke_create_category_async_on_repository()
        {

            var categoryRepositoryMock = new Mock<ICategoryRepository>();
            var mapperMock = new Mock<IMapper>();




            var category = new CategoryDto
            {
                Id = 1,
                CategoryName = "Random Category"
            };

            mapperMock.Setup(x => x.Map<CategoryDto, Category>(It.IsAny<CategoryDto>()))
                .Returns((CategoryDto catDto) =>
                {
                    return new Category
                    {
                        Id = category.Id,
                        CategoryName = category.CategoryName
                    };
                });



            var categoryService = new CategoryService(categoryRepositoryMock.Object, mapperMock.Object);
            await categoryService.CreateCategoryAsync(new CategoryDto { Id = 1, CategoryName = "Sample Category" });


            categoryRepositoryMock.Verify(x => x.CreateCategoryAsync(It.IsAny<Category>()), Times.Once);
        }
    }
}
