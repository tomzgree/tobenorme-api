﻿using FluentAssertions;
using Microsoft.Owin.Testing;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;
using ToBeNorMe.Web;
using Xunit;

namespace ToBeNorMe.Tests.EndToEnd.Controllers
{
    public class CategoryControllerTest
    {
        [Fact]
        public async Task Test()
        {
            var configFile = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile;



            using (var server = TestServer.Create<Startup>())
            {
                var result = await server.HttpClient.GetAsync("api/category");

                string responseContent = await result.Content.ReadAsStringAsync();

                var entity = JsonConvert.DeserializeObject(responseContent);

                entity.ShouldBeEquivalentTo(new object { });
            }





        }
    }
}
