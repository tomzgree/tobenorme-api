﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;


namespace ToBeNorMe.Tests.EndToEnd.Controllers
{

    public class BaseControllerTest
    {
        protected HttpServer _server;
        protected const string Url = "http://localhost:52276/";

        public BaseControllerTest()

        {
            var config = new HttpConfiguration();
            config.Routes.MapHttpRoute(name: "Default", routeTemplate: "api/{controller}/{id}", defaults: new { id = RouteParameter.Optional });




            _server = new HttpServer(config);
            config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;

        }

        protected HttpRequestMessage CreateRequest(string url, string mthv, HttpMethod method)
        {
            var request = new HttpRequestMessage
            {
                RequestUri = new Uri(Url + url)
            };

            request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mthv));
            request.Method = method;

            return request;
        }

        public void Dispose()
        {
            if (_server != null)
            {
                _server.Dispose();
            }
        }


    }
}