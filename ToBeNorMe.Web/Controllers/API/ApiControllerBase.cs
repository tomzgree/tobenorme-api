﻿using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using ToBeNorME.Library.Commands;

namespace ToBeNorMe.Web.Controllers.API
{
    [RoutePrefix("[controller]")]
    public abstract class ApiControllerBase : ApiController
    {
        protected readonly ICommandDispatcher CommandDispatcher;


        protected ApiControllerBase(ICommandDispatcher commandDispatcher)
        {
            {
                CommandDispatcher = commandDispatcher;

                var identity = User.Identity as ClaimsIdentity;
                var claims = identity.Claims;
                var role = identity.Claims.FirstOrDefault(c => c.Type == "https://tobenorme.com/roles");
                if (role != null)
                {
                    identity.AddClaim(new Claim(ClaimTypes.Role, role.Value));
                }
            }

        }
    }
}