﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using ToBeNorMe.Library.Dtos;
using ToBeNorME.Library.Commands;
using ToBeNorME.Library.Commands.Categories;
using ToBeNorME.Library.Services;

namespace ToBeNorMe.Web.Controllers.API
{
    [Authorize(Roles = "Admin")]
    public class CategoryController : ApiControllerBase
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService,
            ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _categoryService = categoryService;

        }

        [HttpGet]
        [AllowAnonymous]
        public async Task<IHttpActionResult> GetCategory(int id)
        {
            var category = await _categoryService.GetCategoryAsync(id);

            if (category == null)
                return NotFound();

            return Ok(category);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetCategoryByName([FromUri]string name)
        {
            var category = await _categoryService.GetCategoryByNameAsync(name);

            if (category == null)
                return NotFound();

            return Ok(category);
        }


        [HttpGet]
        public async Task<IHttpActionResult> GetCategories()
        {
            var categories = await _categoryService.GetAllCategoriesAsync();

            return Ok(categories);
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateCategory(CreateCategory command)
        {

            if (command.Category == null)
                return BadRequest();

            await CommandDispatcher.DispatchAsync(command);

            return Created(new Uri(Request.RequestUri + "/" + command.Category.Id), new object { });

        }

        [HttpPut]
        public async Task<IHttpActionResult> UdpateCategory(int id, CategoryDto categoryDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var categoryInDb = await _categoryService.GetCategoryAsync(id);

            if (categoryInDb == null)
                return NotFound();

            await _categoryService.UpdateCategoryAsync(categoryDto);

            return Created(new Uri(Request.RequestUri + "/" + categoryInDb.Id), categoryInDb);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteCategory(int id)
        {
            await CommandDispatcher.DispatchAsync(new DeleteCategory { Id = id });

            return Ok();
        }
    }
}
