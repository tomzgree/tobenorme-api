﻿using AutoMapper;
using System;
using System.Threading.Tasks;
using System.Web.Http;
using ToBeNorMe.Library.Dtos;
using ToBeNorME.Library.Commands;
using ToBeNorME.Library.Services;

namespace ToBeNorMe.Web.Controllers.API
{
    [Authorize(Roles = "Admin")]
    public class QuestionsController : ApiControllerBase
    {
        private readonly IQuestionService _questionService;
        private readonly IMapper _mapper;

        public QuestionsController(IQuestionService questionService,
            IMapper mapper,
            ICommandDispatcher commandDispatcher) : base(commandDispatcher)
        {
            _questionService = questionService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetQuestions()
        {
            var questionDto = await _questionService.GetAllQuestionsAsync();

            return Ok(questionDto);
        }

        //GET api/questions/1-
        [HttpGet]
        public async Task<IHttpActionResult> GetQuestion(int id)
        {
            var questionDto = await _questionService.GetQuestionAsync(id);

            if (questionDto == null)
                return NotFound();

            return Ok(questionDto);
        }

        [HttpPost]
        public async Task<IHttpActionResult> CreateQuestion(QuestionDto questionDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            await _questionService.CreateQuestionAsync(questionDto);

            return Created(new Uri(Request.RequestUri + "/" + questionDto.Id), questionDto);
        }

        [HttpPut]
        public async Task<IHttpActionResult> UpdateQuestion(int id, QuestionDto questionDto)
        {
            if (!ModelState.IsValid)
                return BadRequest();

            var questionInDb = await _questionService.GetQuestionAsync(id);

            if (questionInDb == null)
                return NotFound();

            var question = _mapper.Map(questionDto, questionInDb);
            await _questionService.UpdateQuestionAsync(question);

            return Created(new Uri(Request.RequestUri + "/" + questionInDb.Id), questionDto);
        }

        [HttpDelete]
        public async Task<IHttpActionResult> DeleteQuestion(int id)
        {
            var questionInDb = await _questionService.GetQuestionAsync(id);

            if (questionInDb == null)
                return NotFound();

            await _questionService.DeleteQuestionAsync(id);

            return Ok($"Question {questionInDb.Description} has been deleted");
        }
    }
}
