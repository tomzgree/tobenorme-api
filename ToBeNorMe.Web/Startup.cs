﻿using Auth0.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Jwt;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Ninject;
using Ninject.Web.Common;
using Ninject.Web.Common.OwinHost;
using Ninject.Web.WebApi.OwinHost;
using Owin;
using System;
using System.Configuration;
using System.IdentityModel.Tokens;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Reflection;
using System.Web;
using System.Web.Http;
using ToBeNorME.Library.Encrytpion;
using ToBeNorME.Library.IoC.Modules;
using ToBeNorME.Library.Repositories;
using ToBeNorME.Library.Services;
using ToBeNorME.Library.Settings;

[assembly: OwinStartup(typeof(ToBeNorMe.Web.Startup))]

namespace ToBeNorMe.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {

            var domain = $"https://{ConfigurationManager.AppSettings["Auth0Domain"]}/";
            var apiIdentifier = ConfigurationManager.AppSettings["Auth0ApiIdentifier"];
            var apiAudience = ConfigurationManager.AppSettings["Auth0Audience"];

            HttpConfiguration config = new HttpConfiguration();

            app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);

            config.Formatters.Remove(config.Formatters.XmlFormatter);

            config.Formatters.JsonFormatter.MediaTypeMappings.Add(new QueryStringMapping("type", "json", new MediaTypeHeaderValue("application/json")));

            //JWT AuthO
            var keyResolver = new OpenIdConnectSigningKeyResolver(domain);

            var jwt = new JwtSecurityToken();
            var claims = jwt.Claims;



            app.UseJwtBearerAuthentication(
            new JwtBearerAuthenticationOptions
            {

                AuthenticationMode = AuthenticationMode.Active,
                TokenValidationParameters = new TokenValidationParameters()
                {

                    ValidAudience = apiAudience,
                    ValidIssuer = domain,
                    IssuerSigningKeyResolver = (token, securityToken, identifier, parameters) => keyResolver.GetSigningKey(identifier)
                }
            });

            var settings = config.Formatters.JsonFormatter.SerializerSettings;

            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            settings.Formatting = Formatting.Indented;

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            //app.UseWebApi(config);
            app.UseNinjectMiddleware(CreateKernel);
            app.UseNinjectWebApi(config);
        }

        private static IKernel CreateKernel()
        {
            var assembly = Assembly.GetAssembly(typeof(CommandModule));

            var inMemory = ConfigurationManager.AppSettings["InMemory"];
            SqlSettings sqlSettings = new SqlSettings
            {
                ConnectionString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
                IsInMemory = Convert.ToBoolean(inMemory)

            };

            var kernel = new StandardKernel(new MappingModule(),
                new CommandModule(),
                new RepositoryModule(),
            new SettingsModule(sqlSettings));
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                //kernel.Bind(x => x.From(assembly).SelectAllClasses().InheritedFrom(typeof(ICommandHandler<>)).BindAllInterfaces());
                //kernel.Bind(x => x.From(assembly).SelectAllInterfaces().InheritedFrom(typeof(IService)).BindToSelf());

                RegisterServices(kernel);

                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        private static void RegisterServices(IKernel kernel)
        {
            kernel.Bind<IQuestionService>().To<QuestionService>();
            kernel.Bind<ICategoryService>().To<CategoryService>();
            kernel.Bind<IEncrypter>().To<Encrypter>().InSingletonScope();
            kernel.Bind<ICategoryRepository>().To<CategoryRepository>();
            kernel.Bind<IQuestionRepository>().To<QuestionRepository>();



        }




    }
}