﻿namespace ToBeNorMe.Web.ViewModels
{
    public class QuestionViewModel
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public string CategoryName { get; set; }
    }
}