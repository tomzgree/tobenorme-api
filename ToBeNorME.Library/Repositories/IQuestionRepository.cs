﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToBeNorME.Library.Models;

namespace ToBeNorME.Library.Repositories
{
    public interface IQuestionRepository : IRepository
    {
        //GET
        Task<Question> GetQuestionAsync(int id);
        //GETALL
        Task<IEnumerable<Question>> GetAllQuestionsAsync();

        Task<IEnumerable<Question>> GetQuestionsByCategoryAsync(int categoryId);
        //CREATE
        Task CreateQuestionAsync(Question question);
        //UPDATE
        Task<Question> UpdateQuestionAsync(Question question);
        //DELETE
        Task DeleteQuestionAsync(int id);
    }
}
