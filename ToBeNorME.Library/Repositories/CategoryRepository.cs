﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using ToBeNorME.Library.Infrastructure;
using ToBeNorME.Library.Models;

namespace ToBeNorME.Library.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly ApplicationDbContext _context;

        public CategoryRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<Category> GetCategoryAsync(int id)
            => await _context.Categories.SingleOrDefaultAsync(c => c.Id == id);

        public async Task<IEnumerable<Category>> GetAllCategoriesAsync()
            => await _context.Categories.ToListAsync();

        public async Task<Category> GetCategoryByNameAsync(string name)
            => await _context.Categories.SingleOrDefaultAsync(c => c.CategoryName == name);

        public async Task CreateCategoryAsync(Category category)
        {
            _context.Categories.Add(category);
            await _context.SaveChangesAsync();
        }

        public async Task<Category> UpdateCategoryAsync(Category category)
        {
            var categoryInDb = await GetCategoryAsync(category.Id);

            if (categoryInDb != null)
            {
                categoryInDb.CategoryName = category.CategoryName;
                await _context.SaveChangesAsync();
            }

            return categoryInDb;

        }

        public async Task DeleteCategoryAsync(Category category)
        {


            if (category != null)
            {
                _context.Categories.Remove(category);
                await _context.SaveChangesAsync();
            }
        }
    }
}
