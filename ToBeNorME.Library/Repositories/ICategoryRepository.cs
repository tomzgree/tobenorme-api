﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToBeNorME.Library.Models;

namespace ToBeNorME.Library.Repositories
{
    public interface ICategoryRepository : IRepository
    {
        //Read
        Task<Category> GetCategoryAsync(int id);
        Task<Category> GetCategoryByNameAsync(string name);
        Task<IEnumerable<Category>> GetAllCategoriesAsync();

        //Create
        Task CreateCategoryAsync(Category category);

        //Update
        Task<Category> UpdateCategoryAsync(Category category);

        //Delete
        Task DeleteCategoryAsync(Category category);
    }
}
