﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using ToBeNorME.Library.Infrastructure;
using ToBeNorME.Library.Models;

namespace ToBeNorME.Library.Repositories
{
    public class QuestionRepository : IQuestionRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public QuestionRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }





        public async Task<Question> GetQuestionAsync(int id)
            => await _context.Questions.SingleOrDefaultAsync(q => q.Id == id);

        public async Task<IEnumerable<Question>> GetAllQuestionsAsync()
            => await _context.Questions.Include(q => q.Category).ToListAsync();

        public async Task<IEnumerable<Question>> GetQuestionsByCategoryAsync(int categoryId)
               => await _context.Questions.Include(q => q.Category).Where(q => q.CategoryId == categoryId).ToListAsync();

        public async Task CreateQuestionAsync(Question question)
        {
            if (question != null)
            {
                _context.Questions.Add(question);
                await _context.SaveChangesAsync();
            }

        }

        public async Task<Question> UpdateQuestionAsync(Question question)
        {
            if (question == null)
                throw new ArgumentNullException();

            var questionInDb = await GetQuestionAsync(question.Id);
            if (questionInDb != null)
            {
                _mapper.Map(question, questionInDb);
                await _context.SaveChangesAsync();
            }

            return questionInDb;
        }

        public async Task DeleteQuestionAsync(int id)
        {
            var questionInDb = await GetQuestionAsync(id);

            _context.Questions.Remove(questionInDb);
            await _context.SaveChangesAsync();
        }

    }
}
