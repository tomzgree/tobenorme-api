namespace ToBeNorME.Library.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<Infrastructure.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "ToBeNorME.Library.Infrastructure.ApplicationDbContext";
        }

        protected override void Seed(ToBeNorME.Library.Infrastructure.ApplicationDbContext context)
        {

            //var manager = new UserManager<User>(new UserStore<User>(new ApplicationDbContext()));

            //var user = new User
            //{
            //    UserName = "SuperPowerUser",
            //    Email = "sup@tobenorme.com",
            //    EmailConfirmed = true,
            //    FullName = "Tomek",
            //    Points = 1,
            //    CreatedAt = DateTime.UtcNow

            //};

            //manager.Create(user, "P@ssw0rd");

            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //
        }
    }
}
