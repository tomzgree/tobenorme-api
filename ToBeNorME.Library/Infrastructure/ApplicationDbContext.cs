﻿using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using ToBeNorME.Library.Models;
using ToBeNorME.Library.Settings;

namespace ToBeNorME.Library.Infrastructure
{
    public class ApplicationDbContext : IdentityDbContext<User>
    {

        private readonly ISettings _settings;

        public ApplicationDbContext() { }

        public ApplicationDbContext(ISettings settings) : base(settings.ConnectionString)
        {
            _settings = settings;
        }

        public DbSet<Question> Questions { get; set; }
        public DbSet<Category> Categories { get; set; }
    }
}
