﻿namespace ToBeNorME.Library.Settings
{
    public interface ISettings
    {
        string ConnectionString { get; set; }
        bool IsInMemory { get; set; }
    }
}