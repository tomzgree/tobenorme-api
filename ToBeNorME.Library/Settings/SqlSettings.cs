﻿namespace ToBeNorME.Library.Settings
{
    public class SqlSettings : ISettings
    {
        //public SqlSettings(ConnectionStringSettings css)
        //{
        //    ConnectionString = css.ConnectionString;
        //}

        public string ConnectionString { get; set; }

        public bool IsInMemory { get; set; }
    }
}
