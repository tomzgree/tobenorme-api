﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;

namespace ToBeNorME.Library.Services
{
    public class InMemoryCategoryService : ICategoryService
    {
        private IList<CategoryDto> _categoryList;

        //private static ISet<Category> _cats = new HashSet<Category>();

        public InMemoryCategoryService()
        {
            _categoryList = new List<CategoryDto>
            {
                new CategoryDto{ Id = 1, CategoryName = "General"},
                new CategoryDto{ Id = 2, CategoryName = "History"}
            };
        }

        public async Task<CategoryDto> GetCategoryAsync(int id)
            => await Task.FromResult(_categoryList.SingleOrDefault(c => c.Id == id));


        public async Task<IEnumerable<CategoryDto>> GetAllCategoriesAsync()
            => await Task.FromResult(_categoryList);

        public async Task<CategoryDto> GetCategoryByNameAsync(string name)
            => await Task.FromResult(_categoryList.SingleOrDefault(c => c.CategoryName == name));

        public async Task CreateCategoryAsync(CategoryDto category)
        {
            category.Id = _categoryList.Count() + 1;
            _categoryList.Add(category);
            await Task.CompletedTask;

        }

        public async Task<CategoryDto> UpdateCategoryAsync(CategoryDto category)
        {
            var categoryInDb = await GetCategoryAsync(category.Id);

            if (categoryInDb != null)
            {
                categoryInDb.Id = category.Id;
                categoryInDb.CategoryName = category.CategoryName;
            }

            return categoryInDb;
        }

        public async Task DeleteCategoryAsync(int id)
        {
            var categoryInDb = await GetCategoryAsync(id);
            await Task.CompletedTask;
        }
    }
}
