﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;
using ToBeNorME.Library.Models;
using ToBeNorME.Library.Repositories;

namespace ToBeNorME.Library.Services
{
    public class CategoryService : ICategoryService
    {
        private readonly ICategoryRepository _categoryRepository;
        private readonly IMapper _mapper;

        public CategoryService(ICategoryRepository categoryRepository, IMapper mapper)
        {
            _categoryRepository = categoryRepository;
            _mapper = mapper;
        }


        public async Task<CategoryDto> GetCategoryAsync(int id)
        {
            var category = await _categoryRepository.GetCategoryAsync(id);

            return _mapper.Map<Category, CategoryDto>(category);
        }

        public async Task<CategoryDto> GetCategoryByNameAsync(string name)
        {
            var category = await _categoryRepository.GetCategoryByNameAsync(name);

            return _mapper.Map<Category, CategoryDto>(category);
        }

        public async Task<IEnumerable<CategoryDto>> GetAllCategoriesAsync()
        {
            var categories = await _categoryRepository.GetAllCategoriesAsync();

            return _mapper.Map<IEnumerable<Category>, IEnumerable<CategoryDto>>(categories);
        }

        public async Task CreateCategoryAsync(CategoryDto categoryDto)
        {
             var category = _mapper.Map<CategoryDto, Category>(categoryDto);

            var categoryInDb = await _categoryRepository.GetCategoryByNameAsync(category.CategoryName);

            if (categoryInDb != null)
                throw new Exception($"Category {categoryInDb.CategoryName} already exists.");

            await _categoryRepository.CreateCategoryAsync(category);
        }

        public async Task<CategoryDto> UpdateCategoryAsync(CategoryDto categoryDto)
        {
            var category = _mapper.Map<CategoryDto, Category>(categoryDto);

            var categoryInDb = await _categoryRepository.GetCategoryAsync(category.Id);

            if (categoryInDb == null)
            {
                throw new Exception($"category doesn't exists.");
            }
            await _categoryRepository.UpdateCategoryAsync(category);

            return _mapper.Map<Category, CategoryDto>(category);
        }

        public async Task DeleteCategoryAsync(int id)
        {
            var category = await _categoryRepository.GetCategoryAsync(id);
            if (category == null)
                throw new Exception($"Can not delete category that doesn't exists.");
            await _categoryRepository.DeleteCategoryAsync(category);
        }

    }
}
