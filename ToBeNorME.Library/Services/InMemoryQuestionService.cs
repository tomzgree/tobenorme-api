﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;

namespace ToBeNorME.Library.Services
{
    public class InMemoryQuestionService : IQuestionService
    {
        private IList<QuestionDto> _questionList;
        public InMemoryQuestionService()
        {
            _questionList = new List<QuestionDto>
            {
                new QuestionDto{ Id = 1, Description = "Is this question stupid?", IsAnswerYes = true, CategoryId = 1},
                new QuestionDto{ Id = 2, Description = "Is it red?", IsAnswerYes = false, CategoryId = 1},
                new QuestionDto{ Id = 3, Description = "Is WWII was started in year 1939?", IsAnswerYes = true, CategoryId = 1}
            };
        }

        public async Task<QuestionDto> GetQuestionAsync(int id)
            => await Task.FromResult(_questionList.SingleOrDefault(q => q.Id == id));

        public async Task<IEnumerable<QuestionDto>> GetAllQuestionsAsync()
            => await Task.FromResult(_questionList);

        public async Task<IEnumerable<QuestionDto>> GetQuestionsByCategoryAsync(int categoryId)
            => await Task.FromResult(_questionList.Where(q => q.CategoryId == categoryId));


        public async Task CreateQuestionAsync(QuestionDto question)
        {
            question.Id = _questionList.Count() + 1;
            _questionList.Add(question);

            await Task.CompletedTask;
        }

        public async Task<QuestionDto> UpdateQuestionAsync(QuestionDto question)
        {
            var questionInDb = await GetQuestionAsync(question.Id);

            if (questionInDb != null)
            {
                questionInDb.Id = question.Id;
                questionInDb.Description = question.Description;
                questionInDb.CategoryId = question.CategoryId;

                questionInDb.IsAnswerYes = question.IsAnswerYes;
            }

            await Task.CompletedTask;

            return questionInDb;
        }

        public async Task DeleteQuestionAsync(int id)
        {

            var questionInDb = await GetQuestionAsync(id);

            if (questionInDb != null)
            {
                _questionList.Remove(questionInDb);
            }
            await Task.CompletedTask;

        }
    }
}
