﻿using System.Threading.Tasks;

namespace ToBeNorME.Library.Services
{
    public interface IDataInitializer
    {
        Task SeedAsync();
    }
}
