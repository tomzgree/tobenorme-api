﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;

namespace ToBeNorME.Library.Services
{
    public interface IQuestionService : IService
    {
        //GET
        Task<QuestionDto> GetQuestionAsync(int id);
        //GETALL
        Task<IEnumerable<QuestionDto>> GetAllQuestionsAsync();

        Task<IEnumerable<QuestionDto>> GetQuestionsByCategoryAsync(int categoryId);
        //CREATE
        Task CreateQuestionAsync(QuestionDto questionDto);
        //UPDATE
        Task<QuestionDto> UpdateQuestionAsync(QuestionDto questionDto);
        //DELETE
        Task DeleteQuestionAsync(int id);
    }
}
