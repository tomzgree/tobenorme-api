﻿using System;
using System.Collections.Generic;
using ToBeNorME.Library.Models;

namespace ToBeNorME.Library.Services
{
    public interface IUserRepository
    {
        User Get(Guid id);
        User Get(string email);
        IEnumerable<User> GetAll();
        void Add(User user);
        void Update(User user);
        void Delete(Guid id);
    }
}
