﻿using AutoMapper;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;
using ToBeNorME.Library.Models;
using ToBeNorME.Library.Repositories;

namespace ToBeNorME.Library.Services
{
    public class QuestionService : IQuestionService
    {
        private readonly IQuestionRepository _questionRepository;
        private readonly IMapper _mapper;

        public QuestionService(IQuestionRepository questionRepository, IMapper mapper)
        {
            _questionRepository = questionRepository;
            _mapper = mapper;
        }





        public async Task<QuestionDto> GetQuestionAsync(int id)
        {
            var question = await _questionRepository.GetQuestionAsync(id);

            return _mapper.Map<Question, QuestionDto>(question);
        }

        public async Task<IEnumerable<QuestionDto>> GetAllQuestionsAsync()
        {
            var question = await _questionRepository.GetAllQuestionsAsync();
            return question.Select(_mapper.Map<Question, QuestionDto>);
        }

        public async Task<IEnumerable<QuestionDto>> GetQuestionsByCategoryAsync(int categoryId)
        {
            var questions = await _questionRepository.GetQuestionsByCategoryAsync(categoryId);
            return questions.Select(_mapper.Map<Question, QuestionDto>);
        }

        public async Task CreateQuestionAsync(QuestionDto questionDto)
        {
            var question = _mapper.Map<QuestionDto, Question>(questionDto);
            await _questionRepository.CreateQuestionAsync(question);
        }

        public async Task<QuestionDto> UpdateQuestionAsync(QuestionDto questionDto)
        {
            var question = _mapper.Map<QuestionDto, Question>(questionDto);
            await _questionRepository.UpdateQuestionAsync(question);
            return questionDto;
        }

        public async Task DeleteQuestionAsync(int id)
        {
            await _questionRepository.DeleteQuestionAsync(id);
        }

    }
}
