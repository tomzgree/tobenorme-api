﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToBeNorMe.Library.Dtos;

namespace ToBeNorME.Library.Services
{
    public interface ICategoryService : IService
    {
        //Read
        Task<CategoryDto> GetCategoryAsync(int id);
        Task<CategoryDto> GetCategoryByNameAsync(string name);
        Task<IEnumerable<CategoryDto>> GetAllCategoriesAsync();

        //Create
        Task CreateCategoryAsync(CategoryDto categoryDto);

        //Update
        Task<CategoryDto> UpdateCategoryAsync(CategoryDto categoryDto);

        //Delete
        Task DeleteCategoryAsync(int id);
    }
}
