﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.ComponentModel.DataAnnotations;

namespace ToBeNorME.Library.Models
{
    public class User : IdentityUser
    {




        [Required]
        public string FullName { get; set; }

        [Required]
        public DateTime CreatedAt { get; set; }

        public int Points { get; set; }





    }
}
