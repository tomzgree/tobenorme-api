﻿namespace ToBeNorME.Library.Models
{
    public class Question
    {

        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsAnswerYes { get; set; }
        public int CategoryId { get; set; }

        public Category Category { get; set; }
    }
}
