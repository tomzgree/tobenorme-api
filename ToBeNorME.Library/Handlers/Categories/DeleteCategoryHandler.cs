﻿using System.Threading.Tasks;
using ToBeNorME.Library.Commands;
using ToBeNorME.Library.Commands.Categories;
using ToBeNorME.Library.Services;

namespace ToBeNorME.Library.Handlers.Categories
{
    public class DeleteCategoryHandler : ICommandHandler<DeleteCategory>
    {
        private readonly ICategoryService _categoryService;

        public DeleteCategoryHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task HandleAsync(DeleteCategory command)
        {
            await _categoryService.DeleteCategoryAsync(command.Id);
        }
    }
}
