﻿using System.Threading.Tasks;
using ToBeNorME.Library.Commands;
using ToBeNorME.Library.Commands.Categories;
using ToBeNorME.Library.Services;

namespace ToBeNorME.Library.Handlers.Categories
{
    public class UpdateCategoryHandler : ICommandHandler<UpdateCategory>
    {
        private readonly ICategoryService _categoryService;

        public UpdateCategoryHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }
        public async Task HandleAsync(UpdateCategory command)
        {
            //await _categoryService.UpdateCategoryAsync();
            await Task.CompletedTask;
        }
    }
}
