﻿using System.Threading.Tasks;
using ToBeNorME.Library.Commands;
using ToBeNorME.Library.Commands.Categories;
using ToBeNorME.Library.Services;

namespace ToBeNorME.Library.Handlers.Categories
{
    public class CreateCategoryHandler : ICommandHandler<CreateCategory>
    {
        private readonly ICategoryService _categoryService;

        public CreateCategoryHandler(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }

        public async Task HandleAsync(CreateCategory command)
        {
            await _categoryService.CreateCategoryAsync(command.Category);
        }
    }
}
