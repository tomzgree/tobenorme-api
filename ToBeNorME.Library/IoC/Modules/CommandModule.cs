﻿using Ninject.Extensions.Conventions;
using Ninject.Modules;
using ToBeNorME.Library.Commands;

namespace ToBeNorME.Library.IoC.Modules
{
    public class CommandModule : NinjectModule
    {

        public override void Load()
        {
            Bind<ICommandDispatcher>().To<CommandDispatcher>().InSingletonScope();
            Kernel.Bind(x => x.FromThisAssembly().SelectAllClasses().InheritedFrom(typeof(ICommandHandler<>)).BindAllInterfaces());
        }
    }
}
