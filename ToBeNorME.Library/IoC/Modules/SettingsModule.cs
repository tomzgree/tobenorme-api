﻿using Ninject.Modules;
using ToBeNorME.Library.Settings;

namespace ToBeNorME.Library.IoC.Modules
{
    public class SettingsModule : NinjectModule
    {
        private readonly SqlSettings _settings;

        public SettingsModule(SqlSettings settings)
        {
            _settings = settings;
        }

        public override void Load()
        {

            Bind<ISettings>().To<SqlSettings>().InThreadScope().WithPropertyValue("ConnectionString", _settings.ConnectionString);

        }
    }
}
