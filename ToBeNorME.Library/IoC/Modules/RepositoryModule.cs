﻿using Ninject.Extensions.Conventions;
using Ninject.Modules;
using ToBeNorME.Library.Repositories;

namespace ToBeNorME.Library.IoC.Modules
{
    public class RepositoryModule : NinjectModule
    {
        public override void Load()
        {
            var assembly = typeof(RepositoryModule).Assembly;

            Kernel.Bind(x => x.From(assembly).SelectAllInterfaces().InheritedFrom(typeof(IRepository)).BindAllBaseClasses());
        }
    }
}
