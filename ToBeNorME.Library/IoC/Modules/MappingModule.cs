﻿using AutoMapper;
using Ninject;
using Ninject.Activation;
using Ninject.Modules;
using ToBeNorMe.Library.Dtos;
using ToBeNorMe.Library.Extensions;
using ToBeNorME.Library.Models;

namespace ToBeNorME.Library.IoC.Modules
{
    public class MappingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(AutoMapper).InSingletonScope();
        }


        private IMapper AutoMapper(IContext context)
        {
            Mapper.Initialize(config =>
            {
                config.ConstructServicesUsing(type => context.Kernel.Get(type));

                config.CreateMap<Question, QuestionDto>()
                .ReverseMap()
                .Ignore(m => m.Id);

                config.CreateMap<Category, CategoryDto>()
                .ReverseMap()
                .Ignore(m => m.Id);
            });

            Mapper.AssertConfigurationIsValid();

            return Mapper.Instance;

        }
    }
}
