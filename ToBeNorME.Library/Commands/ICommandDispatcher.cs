﻿using System.Threading.Tasks;

namespace ToBeNorME.Library.Commands
{
    public interface ICommandDispatcher
    {
        Task DispatchAsync<T>(T command) where T : ICommand;
    }
}
