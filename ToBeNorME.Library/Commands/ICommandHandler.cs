﻿using System.Threading.Tasks;

namespace ToBeNorME.Library.Commands
{
    public interface ICommandHandler<T> where T : ICommand
    {
        Task HandleAsync(T command);
    }
}
