﻿using Ninject;
using System;
using System.Threading.Tasks;

namespace ToBeNorME.Library.Commands
{
    public class CommandDispatcher : ICommandDispatcher
    {
        private readonly IKernel _context;



        public CommandDispatcher(IKernel context)
        {
            _context = context;
        }

        public async Task DispatchAsync<T>(T command) where T : ICommand
        {
            if (command == null)
                throw new ArgumentNullException(nameof(command),
                    $"Command {typeof(T).Name} can not be null");
            var handler = _context.Get<ICommandHandler<T>>();
            await handler.HandleAsync(command);


        }


    }
}
