﻿namespace ToBeNorME.Library.Commands.Categories
{
    public class UpdateCategory : ICommand
    {

        public string CategoryName { get; set; }
    }
}
