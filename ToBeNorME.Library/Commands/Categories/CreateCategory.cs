﻿using ToBeNorMe.Library.Dtos;

namespace ToBeNorME.Library.Commands.Categories
{
    public class CreateCategory : ICommand
    {
        public CategoryDto Category { get; set; }
    }
}
