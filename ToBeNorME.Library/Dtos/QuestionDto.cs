﻿namespace ToBeNorMe.Library.Dtos
{
    public class QuestionDto
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool IsAnswerYes { get; set; }
        public int CategoryId { get; set; }
    }
}